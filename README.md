# README #

Docker repo to build an image with the libraries needed for Eventfinda's
infrastructure.

Installs AWS cli & Composer.

This image is built automatically with Docker Hub.

### Build and Run ##

* Build the image:  
  `$ docker build -t docker-base .`
* Run the image:  
  `$ docker run --rm docker-base`